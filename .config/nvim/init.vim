"""""""""""
" plugins "
"""""""""""
call plug#begin('~/.local/share/nvim/plugged')

Plug 'Yggdroot/indentLine'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-commentary'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'simnalamburt/vim-mundo'
Plug 'rust-lang/rust.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-vinegar'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
Plug 'ap/vim-buftabline'

"themes
Plug 'morhetz/gruvbox'

call plug#end()

"""""""""""
" vim
"""""""""""

autocmd BufEnter * silent! lcd %:p:h

filetype indent on "Enable indentation rules that are file-type specific
syntax on "syntax highlighting

set autoindent "New lines inherit the indentation of previous lines
set autoread "Automatically re-read files if unmodified inside Vim
set autowrite "Automatically save
set backspace=indent,eol,start "Allow backspacing over indention, line breaks and insertion start
set clipboard=unnamed
set colorcolumn=120
set completeopt=menu
set cursorline "Highlight the line currently under cursor
set encoding=utf-8 "Use an encoding that supports unicode
set expandtab "Convert tabs to spaces
set hlsearch "Enable search highlighting
set hidden "Hide files in the background instead of closing them
set inccommand=nosplit
set incsearch "Incremental search that shows partial matches
set laststatus=2 "Always display the status bar
set list
set listchars=tab:▸\ ,trail:·,extends:…,nbsp:␣ " Show soft wrapped lines as …
set mouse=a "Enable mouse for scrolling and resizing
set nobackup
set noshowmode
set number "Show line numbers on the sidebar
set shortmess=a
set showbreak=↳
set showcmd
set showmatch
set softtabstop=2
set splitbelow
set splitright
set shiftround "When shifting lines, round the indentation to the nearest multiple of “shiftwidth”
set shiftwidth=2 "When shifting, indent using 2 spaces
set smarttab
set smartindent
set smarttab "Insert “tabstop” number of spaces when the “tab” key is pressed
set tabstop=2 "Indent using 2 spaces
set title "Set the window’s title, reflecting the file currently being edited
set ttyfast
set undofile " Enable persistent undo so that undo history persists across vim sessions
set undodir=~/.config/nvim/undo
set wrap "Enable line wrapping
if !has('gui_running')
  set t_Co=256
endif
if (has("termguicolors"))
 set termguicolors
endif
"""""""""""
" keymaps
"""""""""""
" leader key
let g:mapleader = ","

""""""""""" buffers
" Close current buffer
noremap <Leader>d :bd<CR>
" cycle buffers
nnoremap <S-left> :bprev<CR>
nnoremap <S-right> :bnext<CR>
" switch to previous buffer
nmap <Leader><Leader> <c-^>

" Highlight all words matching the one under the cursor
noremap <Space> *N
" Clear hilights
noremap <Leader><Space> :noh<cr>

" move lines up or down
nnoremap <c-up>  :<c-u>execute 'move -1-'. v:count1<cr>
nnoremap <c-down>  :<c-u>execute 'move +'. v:count1<cr>

" Reselect visual block after indent/outdent. Allow ident/outdent multiple
" times
vnoremap < <gv
vnoremap > >gv

" gitgutter
nnoremap <leader>gn :GitGutterNextHunk<CR>
nnoremap <leader>gp :GitGutterPrevHunk<CR>
nnoremap <leader>gd :GitGutterPreviewHunk<CR>
nnoremap <leader>gs :GitGutterStageHunk<CR>

" fzf
nnoremap <leader>b :Buffers<cr>
nnoremap <leader>f :GFiles<cr>
nnoremap <leader>h :History<cr>
nnoremap <c-s-f> :Rg<cr>

" languageclient
nnoremap <silent> <leader>lh :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> <leader>ld :call LanguageClient#textDocument_definition()<CR>

"""""""""""
" plugin settings
"""""""""""

" mundo undo tree
nnoremap <F5> :MundoToggle<CR>
let g:mundo_preview_bottom = 1
let g:mundo_close_on_revert = 1

" LanguageClient-neovim
set hidden
let g:LanguageClient_serverCommands = {
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'beta', 'rls'],
    \ }
let g:LanguageClient_autoStart = 1

" deoplete
let g:deoplete#enable_at_startup = 1

""""""""""""""""
" language settings
"""""""""""""""
let g:rustfmt_autosave = 1

""""""
" colourscheme
""""""
set background=dark

let base16colorspace=256
let g:enable_bold_font = 1
let g:enable_italic_font = 1
let g:gruvbox_italic = 1

let g:lightline = {
  \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch' ],
      \             [ 'readonly', 'filename', 'modified' ] ],
      \   'right': [ [ 'percent' ],
      \              [ 'filetype' ] ]
  \ },
  \ 'component_function': {
  \   'gitbranch': 'fugitive#head'
  \ },
  \ }

colorscheme gruvbox
