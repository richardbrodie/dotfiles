[33mcommit 9259738d85e84dbc1a000e37b9988da92f966b57[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 18:48:17 2018 +0200

    VIM: add auto-pairs, replace indent-guides with indentline

[33mcommit ab1a20151caf1cb2c74ed0cd7994b25c8945e688[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 16:01:45 2018 +0200

    VIM: add deoplete and some more configuration for ale to better work with rls

[33mcommit e823b6df99af5d4419fb123c9298111c9c6c9530[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 14:32:02 2018 +0200

    VIM: show current git branch in status bar

[33mcommit 9d432ed57e901a28c35f06478ba6705f0d2509d2[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 13:11:37 2018 +0200

    VIM: set bold and italic fonts to display

[33mcommit 18a311aa3febf48481900ad724043460c3d04f7b[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 12:59:25 2018 +0200

    VIM: add mundo, undo tree

[33mcommit 14cc2b7b39c6c40eb464fea8c4cfa1949727adac[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 12:26:31 2018 +0200

    VIM: add indent-guides

[33mcommit 585e7d4bbe807d2b907ad32ee3bba2e9dde9b94f[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 11:15:04 2018 +0200

    Add nvim conf

[33mcommit cc0fa00955e6c70d196f1f1b55db071972fe67f1[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 11:07:00 2018 +0200

    Add sway config

[33mcommit 78920549b1340803d7d5ea28138c4ec8ec271488[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 10:17:08 2018 +0200

    Add i3 and i3status-rust config

[33mcommit 12bb1d66605e7b4e98853839e0c9729115bf520b[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 10:06:51 2018 +0200

    Add zshrc file

[33mcommit 450cb052e4b36d95b99b5fba32b384ea713a320a[m
Author: richard <richard@radagast.nu>
Date:   Sun Oct 14 09:58:30 2018 +0200

    Add tmux config

[33mcommit 0b73c67b60929eb797c4070482b829ac48e85958[m
Author: Richard Brodie <richard@radagast.nu>
Date:   Sun Oct 14 07:33:44 2018 +0000

    Initial commit
