# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS
setopt SHARE_HISTORY

alias ls='ls --color=auto -al'
alias la='exa -al --time-style=long-iso --colour-scale'
alias v='nvim'

export FZF_DEFAULT_COMMAND='fd --type file --follow --hidden --exclude .git'
export FZF_DEFAULT_OPTS="--ansi"

bindkey -s "\e[25~" ""

EDITOR=/usr/bin/nvim

PATH=$PATH:$HOME/.gem/ruby/2.5.0/bin
bindkey '^R' history-incremental-search-backward

BASE16_SHELL=$HOME/.config/base16-shell/
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

#############
## plugins ##
#############

plugins=(git ssh-agent)
source ~/.zplug/init.zsh

zplug mafredri/zsh-async, from:github
zplug sindresorhus/pure, use:pure.zsh, from:github, as:theme
zplug "junegunn/fzf", as:command, use:bin/fzf-tmux
zplug "junegunn/fzf", use:"shell/*.zsh", as:plugin
zplug "larkery/zsh-histdb"

zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-history-substring-search"

if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

zplug load

# Bind UP and DOWN arrow keys for subsstring search.
if zplug check zsh-users/zsh-history-substring-search; then
  zmodload zsh/terminfo
  bindkey '^[[A' history-substring-search-up
  bindkey '^[[B' history-substring-search-down
fi

if zplug check larkery/zsh-histdb; then
  autoload -Uz add-zsh-hook
  add-zsh-hook preexec _start_timer
  add-zsh-hook precmd  _stop_timer
fi
